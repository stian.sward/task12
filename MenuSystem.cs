﻿using System;
using System.Collections.Generic;
using task12.Datahandling;

namespace task12
{
    class MenuSystem
    {
        string ProgramName { get; set; }
        SupervisorDbContext db;
        Creator creator;
        Reader reader;
        Updater updater;
        Deleter deleter;

        public MenuSystem(SupervisorDbContext db, string programName)
        {
            ProgramName = programName;
            this.db = db;
            creator = new Creator(db, this);
            reader = new Reader(db);
            updater = new Updater(db, this);
            deleter = new Deleter(db, this);
        }

        public void MainMenu()
        {
            List<string> mainMenu = new List<string>()
            {
                "**** MAIN MENU ****",
                "1: Administrate students",
                "2: Administrate professors",
                "3: Administrate courses",
                "4: Print all data",
                "ESC: Exit program"
            };

            PrintMenu(mainMenu);
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        SubmenuStudentAdmin();
                        PrintMenu(mainMenu);
                        break;
                    case ConsoleKey.D2:
                        SubmenuProfessorAdmin();
                        PrintMenu(mainMenu);
                        break;
                    case ConsoleKey.D3:
                        SubmenuCourseAdmin();
                        PrintMenu(mainMenu);
                        break;
                    case ConsoleKey.D4:
                        reader.PrintAllData();
                        Utilities.PressAnyKey();
                        PrintMenu(mainMenu);
                        break;
                    case ConsoleKey.Escape:
                        db.SaveChanges();
                        return;
                    default:
                        break;
                }
            }
        }

        void SubmenuStudentAdmin()
        {
            List<string> submenu = new List<string>()
            {
                "**** STUDENT ADMIN ****",
                "1: Create student",
                "2: Update student",
                "3: Delete student",
                "4: Print all students",
                "ESC: Exit to main menu"
            };

            PrintMenu(submenu);
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        creator.AddStudent();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D2:
                        updater.UpdateStudent();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D3:
                        deleter.DeleteStudent();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D4:
                        reader.PrintStudents();
                        Utilities.PressAnyKey();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }

        void SubmenuProfessorAdmin()
        {
            List<string> submenu = new List<string>()
            {
                "**** PROFESSOR ADMIN ****",
                "1: Create professor",
                "2: Update professor",
                "3: Delete professor",
                "4: Print all professors",
                "ESC: Exit to main menu"
            };

            PrintMenu(submenu);
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        creator.AddProfessor();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D2:
                        updater.UpdateProfessor();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D3:
                        deleter.DeleteProfessor();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D4:
                        reader.PrintProfessors();
                        Utilities.PressAnyKey();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }

        void SubmenuCourseAdmin()
        {
            List<string> submenu = new List<string>()
            {
                "**** COURSE ADMIN ****",
                "1: Create course",
                "2: Update course",
                "3: Delete course",
                "4: Print all courses",
                "ESC: Exit to main menu"
            };

            PrintMenu(submenu);
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        creator.AddCourse();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D2:
                        updater.UpdateCourse();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D3:
                        deleter.DeleteCourse();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.D4:
                        reader.PrintCourses();
                        Utilities.PressAnyKey();
                        PrintMenu(submenu);
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Print the given list of choices as a menu to console
        /// </summary>
        /// <param name="menu"></param>
        public void PrintMenu(List<string> menu)
        {
            Console.Clear();
            Console.WriteLine(ProgramName);
            foreach (string line in menu)
            {
                Console.WriteLine(line);
            }
        }
        /// <summary>
        /// Override method for single-string submenues
        /// </summary>
        /// <param name="submenu"></param>
        public void PrintMenu(string submenu)
        {
            PrintMenu(new List<string>() { submenu });
        }
    }
}
