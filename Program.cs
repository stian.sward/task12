﻿using System;

namespace task12
{
    class Program
    {
        static SupervisorDbContext db;

        static void Main(string[] args)
        {
            db = new SupervisorDbContext();
            // Uncomment to print Professors as JSON to console and exit
            //Console.WriteLine(db.ProfToJson());

            // Uncomment to run main program
            new MenuSystem(db, "******** SUPERVISION ADMINISTRATOR ********").MainMenu();
        }
    }
}
