﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace task12
{
    class SupervisorDbContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=STIAN-DESKTOP\\SQLEXPRESS;Initial Catalog=SupervisorDB;Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Composite keys
            modelBuilder.Entity<Enrollment>().HasKey(sc => new { sc.StudentId, sc.CourseCode });

            // Seed data
            // Courses
            Course kom = new Course { CourseCode = "KOM1069", Name = "Introduction to Stand-Up" };
            Course fot = new Course { CourseCode = "FOT3030", Name = "Advanced Nutmegging" };
            Course econ = new Course { CourseCode = "ECON3069", Name = "Advanced Grifting" };
            modelBuilder.Entity<Course>().HasData(kom);
            modelBuilder.Entity<Course>().HasData(fot);
            modelBuilder.Entity<Course>().HasData(econ);

            // Professors
            Professor dag = new Professor { Id = 1, FirstName = "Dag", LastName = "Langmyr" };
            Professor tom = new Professor { Id = 2, FirstName = "Tom", LastName = "Norli" };
            Professor ray = new Professor { Id = 3, FirstName = "Raymond", LastName = "Kvisvik" };
            modelBuilder.Entity<Professor>().HasData(dag);
            modelBuilder.Entity<Professor>().HasData(tom);
            modelBuilder.Entity<Professor>().HasData(ray);
            dag.CourseCode = kom.CourseCode;
            tom.CourseCode = fot.CourseCode;
            kom.LecturerId = dag.Id;
            fot.LecturerId = tom.Id;

            // Students
            Student alex = new Student { Id = 1, FirstName = "Alex", LastName = "Rosén" };
            Student chris = new Student { Id = 2, FirstName = "Christoffer", LastName = "Schau" };
            Student ron = new Student { Id = 3, FirstName = "Ronny", LastName = "Johnsen" };
            modelBuilder.Entity<Student>().HasData(alex);
            modelBuilder.Entity<Student>().HasData(chris);
            modelBuilder.Entity<Student>().HasData(ron);
            alex.SupervisorId = dag.Id;
            ron.SupervisorId = ray.Id;

            // Enrollments
            Enrollment fotEn = new Enrollment { CourseCode = "FOT3030", StudentId = 3 };
            Enrollment komEn = new Enrollment { CourseCode = "KOM1069", StudentId = 1 };
            modelBuilder.Entity<Enrollment>().HasData(fotEn);
            modelBuilder.Entity<Enrollment>().HasData(komEn);
        }

        public string ProfToJson()
        {
            return JsonConvert.SerializeObject(Professors.Include(p => p.Supervisees).Include(p => p.Course),
                Formatting.Indented, new JsonSerializerSettings()
            {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }
    }
}
