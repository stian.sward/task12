﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace task12
{
    class Course
    {
        // Static properties for neat table printing
        public static string Header
        {
            get => TableLine
                + String.Format("\n|{0,56}COURSES{0,51}|\n", "")
                + TableLine
                + String.Format("\n|  {0,-10}  |  {1,-50}  |  {2,-40}  |\n", "CourseCode", "Course Name", "Lecturer")
                + TableLine;
        }
        public static string TableLine
        {
            get => "+" + new string('-', 14) + "+" + new string('-', 54) + "+" + new string('-', 44) + "+";
        }

        [Key, MaxLength(10)] // Primary Key
        public string CourseCode { get; set; }

        // Fields
        public string Name { get; set; }

        // Relationships
        public ICollection<Enrollment> Enrollments { get; set; }

        //      Foreign key to Professor
        public int? LecturerId { get; set; }
        public Professor? Lecturer { get; set; }

        public Course()
        {
            Enrollments = new List<Enrollment>();
        }

        public new string ToString()
        {
            return CourseCode + " - " + Name;
        }

        public string Summary()
        {
            string lecturer = (Lecturer != null) ? Lecturer.ToString() : "N/A";
            return "Course code: " + CourseCode
                + "\nCourse name: " + Name
                + "\nLecturer: " + lecturer;
        }

        public string ToTable()
        {
            return String.Format("|  {0,-10}  |  {1,-50}  |  {2,-40}  |", CourseCode, Name, (Lecturer != null) ? Lecturer.ToString() : "N/A");
        }
    }
}
