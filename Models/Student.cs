﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace task12
{
    class Student
    {
        // Static properties for neat table printing
        public static string Header
        {
            get =>
                "+" + new string('-', 90) + "+"
                + String.Format("\n|{0,40}STUDENTS{0,42}|\n", "")
                + TableLine
                + String.Format("\n|  {0,-4}  |  {1,-36}  |  {2,-36}  |\n", "ID", "Name", "Supervisor")
                + TableLine;
        }
        public static string TableLine
        {
            get => "+" + new string('-', 8) + "+" + new string('-', 40) + "+" + new string('-', 40) + "+";
        }

        [Key] // Primary Key
        public int Id { get; set; }

        // Fields
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        // Relationships
        public ICollection<Enrollment> Enrollments { get; set; }
        //      Foreign key to Professor
        public int? SupervisorId { get; set; }
        public Professor? Supervisor { get; set; }


        public Student()
        {
            Enrollments = new List<Enrollment>();
        }

        public string Summary()
        {
            string supervisor = (Supervisor != null) ? Supervisor.ToString() : "N/A";
            return "First name: " + FirstName
                + "\nLast name: " + LastName
                + "\nSupervisor: " + supervisor;
        }

        public new string ToString()
        {
            return FirstName + " " + LastName;
        }

        public string ToTable()
        {
            return String.Format("|  {0,-4}  |  {1,-36}  |  {2,-36}  |", Id, ToString(), (Supervisor != null) ? Supervisor.ToString() : "N/A");
        }
    }
}
