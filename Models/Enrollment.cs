﻿using System;
using System.ComponentModel.DataAnnotations;

namespace task12
{
    class Enrollment
    {
        // Static properties for neat table printing
        public static string Header
        {
            get =>
                TableLine
                + String.Format("\n|{0,29}ENROLLMENTS{0,29}|\n", "")
                + TableLine
                + String.Format("\n|  {0,-10}  |  {1,-50}  |\n", "Course", "Student")
                + TableLine;
        }
        public static string TableLine
        {
            get => "+" + new string('-', 14) + "+" + new string('-', 54) + "+";
        }

        [Key, MaxLength(10)]    // Primary Key, Foreign Key to Course
        public string CourseCode { get; set; }
        [Key]                   // Primary Key, Foreign Key to Student
        public int StudentId { get; set; }

        public Student Student { get; set; }
        public Course Course { get; set; }

        public string ToTable()
        {
            return String.Format("|  {0,-10}  |  {1,-50}  |", Course.CourseCode, Student.ToString());
        }
    }
}
