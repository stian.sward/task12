﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace task12
{
    class Professor
    {
        // Static properties for neat table printing
        public static string Header
        {
            get =>
                "+" + new string('-', 90) + "+"
                + String.Format("\n|{0,40}PROFESSORS{0,40}|\n", "")
                + TableLine
                + String.Format("\n|  {0,-4}  |  {1,-36}  |  {2,-36}  |\n", "ID", "Name", "Lectures")
                + TableLine;
        }
        public static string TableLine
        {
            get => "+" + new string('-', 8) + "+" + new string('-', 40) + "+" + new string('-', 40) + "+";
        }
        [Key] // Primary Key
        public int Id { get; set; }

        // Fields
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        // Relationships
        [MaxLength(10)]
        public string CourseCode { get; set; }
        public Course Course { get; set; }
        public ICollection<Student> Supervisees { get; set; }

        public Professor()
        {
            Supervisees = new List<Student>();
        }

        public string Summary()
        {
            string course = (Course != null) ? Course.CourseCode : "N/A";
            return "Professor ID: " + Id
                + "\nFirst name: " + FirstName
                + "\nLast name: " + LastName
                + "\nLectures: " + course;
        }

        public string ToTable()
        {
            return String.Format("|  {0,-4}  |  {1,-36}  |  {2,-36}  |", Id, ToString(), (Course != null) ? Course.ToString() : "N/A");
        }

        public new string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
