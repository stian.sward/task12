﻿using System;
using System.Linq;

namespace task12.Datahandling
{
    class Updater
    {
        SupervisorDbContext db;
        MenuSystem menu;

        public Updater(SupervisorDbContext db, MenuSystem menu)
        {
            this.db = db;
            this.menu = menu;
        }

        /// <summary>
        /// Allows the user to edit the data related to a course given by a Course Code
        /// </summary>
        public void UpdateCourse()
        {
            menu.PrintMenu("**** UPDATE COURSE ****");

            // Fetch Course
            string input;
            Course course;
            do
            {
                input = Utilities.GetString("Enter course code to update, or leave blank to exit: ");
                if (input == "") return;
                course = db.Courses.Find(input);
                if (course == null) Console.WriteLine("Couldn't find that course. Try again.");
            } while (course == null);

            // Print and update data
            Console.WriteLine(course.Summary());
            Console.WriteLine("Enter new data, or leave blank to skip");

            // Course Code
            while (true)
            {
                input = Utilities.GetString("Course code: ");
                if (input != "" && input != course.CourseCode)
                {
                    if (db.Courses.Find(input) != null)
                    {
                        Console.WriteLine("That course already exists. CourseCode must be unique.");
                    }
                    else
                    {
                        course.CourseCode = input;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            // Course Name
            input = Utilities.GetString("Course name: ");
            if (input != "") course.Name = input;

            // Change lecturer
            input = Utilities.GetString("Lecturer ID: ");
            try
            {
                int id = int.Parse(input);
                Professor prof = db.Professors.Find(id);
                if (prof != null)
                {
                    prof.Course = course;
                    prof.CourseCode = course.CourseCode;
                    course.Lecturer = prof;
                    course.LecturerId = prof.Id;
                }
                else
                {
                    Console.WriteLine("Couldn't find that professor");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // Save and confirm to console
            db.SaveChanges();
            Console.WriteLine(course.CourseCode + " has been updated!");
            Utilities.PressAnyKey();
        }

        /// <summary>
        /// Allows the user to edit the data related to a Professor given by the Professor's ID
        /// </summary>
        public void UpdateProfessor()
        {
            menu.PrintMenu("**** UPDATE PROFESSOR ****");

            // Fetch Professor
            int id;
            Professor prof;
            string input;
            do
            {
                id = Utilities.GetPositiveInt("Enter Professor ID, or leave blank to exit: ");
                if (id == 0) return;
                prof = db.Professors.Find(id);
                if (prof == null) Console.WriteLine("Couldn't find that Professor. Try again.");
            } while (prof == null);

            Console.WriteLine(prof.Summary());
            Console.WriteLine("Enter new data, or leave blank to skip");

            // Update Name
            input = Utilities.GetString("First name: ");
            if (input != "") prof.FirstName = input;
            input = Utilities.GetString("Last name: ");
            if (input != "") prof.LastName = input;

            // Update the professor' lecturing position
            Course newCourse = null;
            do
            {
                input = Utilities.GetString("Enter the course code this professor has lectures in, or leave blank to skip/remove: ");
                if (input == "")
                {
                    if (prof.Course != null)
                    {
                        prof.Course.Lecturer = null;
                        prof.Course.LecturerId = null;
                        prof.Course = null;
                        prof.CourseCode = null;
                    }
                }
                else if ((newCourse = db.Courses.Find(input)) != null)
                {
                    if (prof.Course != null)
                    {
                        prof.Course.Lecturer = null;
                        prof.Course.LecturerId = null;
                    }
                    prof.Course = newCourse;
                    prof.CourseCode = newCourse.CourseCode;
                    newCourse.Lecturer = prof;
                    newCourse.LecturerId = prof.Id;
                }
                else
                {
                    Console.WriteLine("Couldn't find that course. Try again.");
                }
            } while (newCourse == null && input != "");

            // Save and print confirmation
            db.SaveChanges();
            Console.WriteLine("Updated " + prof.ToString() + ".");
            Utilities.PressAnyKey();
        }

        /// <summary>
        /// Allows the user to edit the data related to a Student, including Enrollments, given the Student's ID
        /// </summary>
        public void UpdateStudent()
        {
            menu.PrintMenu("**** UPDATE STUDENT ****");

            // Fetch Student
            int id;
            Student student;
            Course course;
            Enrollment enrollment;
            Professor supervisor;
            string input;
            do
            {
                id = Utilities.GetPositiveInt("Enter Student ID, or leave blank to exit: ");
                if (id == 0) return;
                student = db.Students.Find(id);
                if (student == null) Console.WriteLine("Couldn't find that Student. Try again.");
            } while (student == null);

            // Print and update data
            Console.WriteLine(student.Summary());
            Console.WriteLine("Enter new data, or leave blank to skip");
            input = Utilities.GetString("First name: ");
            if (input != "") student.FirstName = input;
            input = Utilities.GetString("Last name: ");
            if (input != "") student.LastName = input;

            // Update supervisor
            if (student.Supervisor != null)
            {
                Console.WriteLine(student.ToString() + " is currently supervised by " + student.Supervisor.ToString());
            }
            else
            {
                Console.WriteLine(student.ToString() + " does not currenly have a supervisor.");
            }
            id = Utilities.GetPositiveInt("Enter the new supervisor's ID, or leave blank to skip changing: ");
            while ((supervisor = db.Professors.Find(id)) == null && id != 0)
            {
                id = Utilities.GetPositiveInt("Couldn't find that Professor. Try again: ");
            }
            if (supervisor != null)
            {
                supervisor.Supervisees.Add(student);
                student.Supervisor = supervisor;
                student.SupervisorId = supervisor.Id;
            }

            // Update enrollments
            if (student.Enrollments.Count > 0)
            {
                Console.WriteLine(student.ToString() + " is currently enrolled in the following courses:");
                foreach (Enrollment enr in student.Enrollments)
                {
                    Console.WriteLine(enr.Course.ToString());
                }
            }
            else
            {
                Console.WriteLine(student.ToString() + " is not currently enrolled in any courses.");
            }

            input = Utilities.GetString("Enter an existing Course Code to delete the enrollment, a new one to add, or leave blank to skip changes: ");
            while (input != "")
            {
                course = db.Courses.Find(input);
                if (course != null)
                {

                    if ((enrollment = db.Enrollments.Where(e => e.Student == student && e.Course == course).SingleOrDefault()) != null)
                    {
                        // Student is already enrolled, delete enrollment
                        db.Enrollments.Remove(enrollment);
                        course.Enrollments.Remove(enrollment);
                        student.Enrollments.Remove(enrollment);
                    }
                    else
                    {
                        // Student is not enrolled, create enrollment
                        enrollment = new Enrollment()
                        {
                            Course = course,
                            CourseCode = course.CourseCode,
                            Student = student,
                            StudentId = student.Id
                        };
                        student.Enrollments.Add(enrollment);
                        course.Enrollments.Add(enrollment);
                    }
                }
                else
                {
                    Console.WriteLine("That course doesn't exist.");
                }
                input = Utilities.GetString("Enter another course, or leave blank to finish: ");
            }

            db.SaveChanges();
            Console.WriteLine("Updated " + student.ToString());
            Utilities.PressAnyKey();
        }
    }
}
