﻿using System;

namespace task12.Datahandling
{
    class Creator
    {
        SupervisorDbContext db;
        MenuSystem menu;

        public Creator(SupervisorDbContext db, MenuSystem menu)
        {
            this.db = db;
            this.menu = menu;
        }

        /// <summary>
        /// Add a new course to the database
        /// </summary>
        public void AddCourse()
        {
            menu.PrintMenu("**** ADD COURSE ****");
            string courseCode = Utilities.GetString("Course code: ");
            while (db.Courses.Find(courseCode) != null)
            {
                courseCode = Utilities.GetString("A course with that code already exists\nTry again: ");
            }
            string courseName = Utilities.GetString("Course name: ");
            Course course = new Course()
            {
                CourseCode = courseCode,
                Name = courseName
            };

            // Add optional lecturer
            string input = Utilities.GetString("Add lecturer's ID (or leave blank to add later): ");
            if (input != "")
            {
                try
                {
                    int lecturerId = int.Parse(input);
                    Professor prof = db.Professors.Find(lecturerId);
                    if (prof != null)
                    {
                        course.LecturerId = lecturerId;
                        course.Lecturer = prof;
                    }
                    else
                    {
                        Console.WriteLine("That professor doesn't exist.\nCreate the professor, then add them to the course.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Dropping lecturer");
                }
            }

            db.Courses.Add(course);
            db.SaveChanges();
            Console.WriteLine("\nAdded " + courseCode + " to Courses!");
            Utilities.PressAnyKey();
        }

        /// <summary>
        /// Add a new professor to the database
        /// </summary>
        public void AddProfessor()
        {
            menu.PrintMenu("**** ADD PROFESSOR ****");
            string firstName = Utilities.GetString("First name: ");
            string lastName = Utilities.GetString("Last name: ");
            Professor prof = new Professor()
            {
                FirstName = firstName,
                LastName = lastName
            };
            db.Professors.Add(prof);

            // Add optional lecturer position
            string input;
            Course course;
            do
            {
                input = Utilities.GetString("Enter the course code this professor has lectures in, or leave blank to skip: ");
                course = db.Courses.Find(input);
                if (course != null && input != "")
                {
                    course.Lecturer = prof;
                    course.LecturerId = prof.Id;
                    prof.CourseCode = course.CourseCode;
                    prof.Course = course;
                }
                else
                {
                    Console.WriteLine("Could not find that course. Try again.");
                }
            } while (course == null && input != "");

            db.SaveChanges();
            Console.WriteLine("\nAdded " + firstName + " " + lastName + " to Professors!");
            Utilities.PressAnyKey();
        }

        /// <summary>
        /// Add a new student to the database
        /// </summary>
        public void AddStudent()
        {
            menu.PrintMenu("**** ADD STUDENT ****");
            string firstName = Utilities.GetString("First name: ");
            string lastName = Utilities.GetString("Last name: ");
            Student stud = new Student()
            {
                FirstName = firstName,
                LastName = lastName
            };
            db.Students.Add(stud);

            // Add optional supervisor
            int supervisorId;
            Professor supervisor;
            do
            {
                supervisorId = Utilities.GetPositiveInt("Enter supervisor ID, or leave blank to skip: ");
                supervisor = db.Professors.Find(supervisorId);
                if (supervisor == null)
                {
                    Console.WriteLine("Couldn't find that professor. Try again.");
                }
            } while (supervisor == null && supervisorId > 0);
            if (supervisor != null)
            {
                supervisor.Supervisees.Add(stud);
                stud.Supervisor = supervisor;
                stud.SupervisorId = supervisor.Id;
            }

            // Add optional enrollments
            string input = Utilities.GetString("Enter course code to add enrollment, or leave blank to skip: ");
            while (input != "")
            {
                Course course = db.Courses.Find(input);
                if (course != null)
                {
                    Enrollment att = new Enrollment()
                    {
                        StudentId = stud.Id,
                        CourseCode = course.CourseCode
                    };
                    stud.Enrollments.Add(att);
                    course.Enrollments.Add(att);
                    db.Enrollments.Add(att);
                    Console.WriteLine(firstName + " " + lastName + " is now enrolled in " + course.CourseCode);
                }
                else
                {
                    Console.WriteLine("Couldn't find that course.");
                }
                input = Utilities.GetString("Enter a new course code, or leave blank: ");
            }

            db.SaveChanges();
            Console.WriteLine("Added " + firstName + " " + lastName + " to Students!");
            Utilities.PressAnyKey();
        }
    }
}
