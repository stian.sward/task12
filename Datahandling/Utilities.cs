﻿using System;

namespace task12.Datahandling
{
    class Utilities
    {
        /// <summary>
        /// Prompts the user for input, and returns the given string
        /// </summary>
        /// <param name="prompt">
        /// The message written to console to describe the input
        /// </param>
        /// <returns>
        /// User's input as string object
        /// </returns>
        public static string GetString(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine();
        }

        /// <summary>
        /// Asks the user for input until given a valid positive integer or no input
        /// </summary>
        /// <param name="prompt">Message describing the desired input, or 0 if no input is given</param>
        /// <returns>The integer-parsed input</returns>
        public static int GetPositiveInt(string prompt)
        {
            string input;
            int res = 0;
            Console.Write(prompt);
            do
            {
                try
                {
                    input = Console.ReadLine();
                    if (input == "") return 0;

                    res = int.Parse(input);
                    if (res < 1)
                    {
                        Console.Write("Must be 1 or higher\nTry again: ");
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message + "\nTry again: ");
                }
            } while (res < 1);
            return res;
        }

        /// <summary>
        /// Waits for the user to provide input to continue the program
        /// </summary>
        public static void PressAnyKey()
        {
            Console.WriteLine("\nPress any key to return...");
            Console.ReadKey(true);
        }
    }
}
