﻿using Microsoft.EntityFrameworkCore;
using System;

namespace task12.Datahandling
{
    class Reader
    {
        SupervisorDbContext db;

        public Reader(SupervisorDbContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Prints the DbSets in the DbContext object to neat little tables in console
        /// </summary>

        public void PrintCourses()
        {
            Console.WriteLine(Course.Header);
            foreach (Course course in db.Courses)
            {
                Console.WriteLine(course.ToTable());
            }
            Console.WriteLine(Course.TableLine);
        }

        public void PrintProfessors()
        {
            Console.WriteLine(Professor.Header);
            foreach (Professor prof in db.Professors.Include(p => p.Course))
            {
                Console.WriteLine(prof.ToTable());
            }
            Console.WriteLine(Professor.TableLine);
        }

        public void PrintStudents()
        {
            Console.WriteLine(Student.Header);
            foreach (Student student in db.Students.Include(s => s.Supervisor))
            {
                Console.WriteLine(student.ToTable());
            }
            Console.WriteLine(Student.TableLine);
        }

        public void PrintEnrollments()
        {
            Console.WriteLine(Enrollment.Header);
            foreach (Enrollment att in db.Enrollments)
            {
                Console.WriteLine(att.ToTable());
            }
            Console.WriteLine(Enrollment.TableLine);
        }

        public void PrintAllData()
        {
            PrintStudents();
            Console.WriteLine();
            PrintProfessors();
            Console.WriteLine();
            PrintCourses();
            Console.WriteLine();
            PrintEnrollments();
            Console.WriteLine();
        }
    }
}
