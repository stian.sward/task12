﻿using System;

namespace task12.Datahandling
{
    class Deleter
    {
        SupervisorDbContext db;
        MenuSystem menu;

        public Deleter(SupervisorDbContext db, MenuSystem menu)
        {
            this.db = db;
            this.menu = menu;
        }

        public void DeleteCourse()
        {
            menu.PrintMenu("**** DELETE COURSE ****");

            // Fetch Course
            string input;
            Course course;
            do
            {
                input = Utilities.GetString("Enter course code to update, or leave blank to exit: ");
                if (input == "") return;
                course = db.Courses.Find(input);
                if (course == null) Console.WriteLine("Couldn't find that course. Try again.");
            } while (course == null);

            // Remove from database and db.Courses
            db.Courses.Remove(course);
            db.SaveChanges();
            Console.WriteLine("Deleted " + course.ToString() + " from table");
            Utilities.PressAnyKey();
        }

        public void DeleteProfessor()
        {
            menu.PrintMenu("**** DELETE PROFESSOR ****");

            // Fetch Professor
            int id;
            Professor prof;
            do
            {
                id = Utilities.GetPositiveInt("Enter Professor ID, or leave blank to exit: ");
                if (id == 0) return;
                prof = db.Professors.Find(id);
                if (prof == null) Console.WriteLine("Couldn't find that Professor. Try again.");
            } while (prof == null);

            // Remove from database and db.Professors
            db.Professors.Remove(prof);
            db.SaveChanges();
            Console.WriteLine("Deleted " + prof.ToString() + " from table");
            Utilities.PressAnyKey();
        }

        public void DeleteStudent()
        {
            menu.PrintMenu("**** DELETE STUDENT ****");

            // Fetch Student
            int id;
            Student stud;
            do
            {
                id = Utilities.GetPositiveInt("Enter Professor ID, or leave blank to exit: ");
                if (id == 0) return;
                stud = db.Students.Find(id);
                if (stud == null) Console.WriteLine("Couldn't find that Student. Try again.");
            } while (stud == null);

            // Remove from database and sb.Students
            db.Students.Remove(stud);
            db.SaveChanges();
            Console.WriteLine("Deleted " + stud.ToString() + " from table.");
            Utilities.PressAnyKey();
        }
    }
}
