# task12

## A system for entering, updating and deleting information about Professors, their lecturer positions, Courses, Students and their Enrollments, and supervision relationships between Students and Professors.

* The program is entirely command-line based. There is a menu-system based on key presses corresponding to the various menu choices, the program responds immediately to input.

* The program is not particularly focused on database queries; its main functions are adding and updating data.