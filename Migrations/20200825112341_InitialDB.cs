﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace task12.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    CourseCode = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    CourseCode = table.Column<string>(maxLength: 10, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    LecturerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.CourseCode);
                    table.ForeignKey(
                        name: "FK_Courses_Professors_LecturerId",
                        column: x => x.LecturerId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    SupervisorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Professors_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enrollments",
                columns: table => new
                {
                    CourseCode = table.Column<string>(maxLength: 10, nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enrollments", x => new { x.StudentId, x.CourseCode });
                    table.ForeignKey(
                        name: "FK_Enrollments_Courses_CourseCode",
                        column: x => x.CourseCode,
                        principalTable: "Courses",
                        principalColumn: "CourseCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Enrollments_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "CourseCode", "LecturerId", "Name" },
                values: new object[] { "ECON3069", null, "Advanced Grifting" });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "CourseCode", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "KOM1069", "Dag", "Langmyr" },
                    { 2, "FOT3030", "Tom", "Norli" },
                    { 3, null, "Raymond", "Kvisvik" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "FirstName", "LastName", "SupervisorId" },
                values: new object[] { 2, "Christoffer", "Schau", null });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "CourseCode", "LecturerId", "Name" },
                values: new object[,]
                {
                    { "KOM1069", 1, "Introduction to Stand-Up" },
                    { "FOT3030", 2, "Advanced Nutmegging" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "FirstName", "LastName", "SupervisorId" },
                values: new object[,]
                {
                    { 1, "Alex", "Rosén", 1 },
                    { 3, "Ronny", "Johnsen", 3 }
                });

            migrationBuilder.InsertData(
                table: "Enrollments",
                columns: new[] { "StudentId", "CourseCode" },
                values: new object[] { 1, "KOM1069" });

            migrationBuilder.InsertData(
                table: "Enrollments",
                columns: new[] { "StudentId", "CourseCode" },
                values: new object[] { 3, "FOT3030" });

            migrationBuilder.CreateIndex(
                name: "IX_Courses_LecturerId",
                table: "Courses",
                column: "LecturerId",
                unique: true,
                filter: "[LecturerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enrollments_CourseCode",
                table: "Enrollments",
                column: "CourseCode");

            migrationBuilder.CreateIndex(
                name: "IX_Students_SupervisorId",
                table: "Students",
                column: "SupervisorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enrollments");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Professors");
        }
    }
}
